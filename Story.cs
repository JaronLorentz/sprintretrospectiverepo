﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    public interface IStory
    {
        List<Story> Stories { get; set; }
        double GetTotalHours();
        void AddTask(string task, bool state);
    }

    public class Story : IStory
    {
        public List<Story> Stories { get; set; }
        public enum States { Unstarted, Started, Finished, Delivered, Rejected, Accepted };
        public enum StoryTypes { Feature, Bug, Choice, Release }
        public string UsrStory { get; set; }
        public int OriginalStorypoints { get; set; }
        public double OriginalEstimatedHours { get; set; }
        public List<Tuple<User,double>> Members { get; set; }
        public double ReEstimateToComplete { get; set; }
        public States State { get; set; }
        public StoryTypes StoryType { get; set; }
        public List<Tuple<string, bool>> Task { get; set; }

        public double GetTotalHours()
        {
            double hours = 0;
            for (int i = 0; i < Members.Count; i++)
            {
                hours += Members.ElementAt(i).Item2;
            }
            return hours;
        }

        public void AddTask(string task, bool state)
        {
            Task.Add(new Tuple<string, bool>(task, state));
        }

    }
}
