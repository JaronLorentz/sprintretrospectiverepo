﻿using System;

namespace SprintRetrospective
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime UserCreated { get; set; }
        public string Email { get; set; }

        User(string FirstName_, string LastName_, string Email_)
            { FirstName = FirstName_; LastName = LastName_; Email = Email_; UserCreated = DateTime.Now; }
    }
}
